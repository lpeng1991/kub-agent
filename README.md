# kub-agent

Agent 包括两部分:
- 服务端，位于 GitLab 一侧，简称:kas
- 客户端，位于 k8s 一侧，简称:agentk

要基于 Agent 执行 GitOps 部署，需要满足下列条件:
- 一个配置好的 k8s 集群，安装了 Agent 客户端; 
- GitLab 启用了 Agent 服务端;
- GitLab 创建了 Agent 配置库和清单库;

![image.png](./image.png)

配置文件
- .gitlab/agents/agent1/config.yaml

清单文件
- deploy/nginx.yaml
